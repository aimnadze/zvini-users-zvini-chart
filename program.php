#!/usr/bin/php
<?php

chdir(__DIR__);
include_once 'config.php';

if ($verbose) {
    echo "INFO: Fetching the quantity of users from $from[zvini_location]\n";
}

$ch = curl_init();
curl_setopt_array($ch, [
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_URL => "$from[zvini_location]admin/api-call/user/list",
    CURLOPT_POSTFIELDS => http_build_query([
        'admin_api_key' => $from['admin_api_key'],
    ]),
]);
$response = curl_exec($ch);

if ($response === false) {
    echo 'ERROR: '.curl_error($ch)."\n";
    exit(1);
}
if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
    echo "ERROR: $response\n";
    exit(1);
}

$response = json_decode($response);

if ($verbose) {
    echo "INFO: Updating bar chart #$to[id]\n";
}

$ch = curl_init();
curl_setopt_array($ch, [
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_URL => "$to[zvini_location]api-call/barChart/bar/add",
    CURLOPT_POSTFIELDS => http_build_query([
        'api_key' => $to['api_key'],
        'id' => $to['id'],
        'label' => date('Y-m-d'),
        'value' => count($response),
    ]),
]);
$response = curl_exec($ch);

if ($response === false) {
    echo 'ERROR: '.curl_error($ch)."\n";
    exit(1);
}
if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
    echo "ERROR: $response\n";
    exit(1);
}
