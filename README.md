Zvini Users Zvini Chart
=======================

The program reads how many users are signed up in a
[Zvini](https://gitlab.com/zvini/website) instance and appends the number
to a bar chart that is located on the same or on another instance of Zvini.
